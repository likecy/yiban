###此为网站目录放置二级程序目录使用


- 进入yibanmain/public目录，把添加的二级程序的目录软连接到里面，文件名即二级程序的访问url。

- 软连接制作流程：mklink /J 二级程序别名 二级程序实际地址；



###Forexample：

    比方我现在有二级网站程序bbs，在此目录bbs文件夹里放程序，进入yibanadmin/public目录，把bbs程序的入口文件夹软连接进去改名为bbs。
     ☞ cmd打开路径：cd D:\wwwroot\yiban.mnu.cn\yibanadmin\public
     ☞ CMD管理员方式键入：mklink /J bbs ../../bbs
     ☞ 在bbs文件夹里面配置好路由重写规则后即可实现二级程序路由；
     ☞ bbs程序中有路由控制器yiban；现在可以通过http://yiban.mnu.cn/bbs/yiban进行访问。
     ☞ bbs程序apache重写配置：


```
          <IfModule mod_rewrite.c>
             RewriteEngine on
             RewriteCond %{REQUEST_FILENAME} !-d
             RewriteCond %{REQUEST_FILENAME} !-f
             RewriteRule ^(.*)$ /bbs/index.php?s=/$1 [QSA,PT,L]
         </IfModule>


```



###易班登录接入系统：
1. composer 安装我封装好的包
   cmd键入:composer require likecy/yiban:dev-master
（具体使用流程请参照我的码云yibanSDK接入API地址：https://gitee.com/likecy/yiban）


2.如何实际接入请参考控制器

#####YibanApi 模型文件：

```php
<?php
/**
 * Created by PhpStorm.
 * User: chenyun
 * Date: 2019-02-18
 * Time: 11:09
 */

namespace app\index\model;


use likecy\yiban\YBOpenApi;
use think\Model;

class YibanAPI extends  Model
{
      //第一次登录调用设置token
    public static function LoginSetToken(){
       
        $yibanAPi= YBOpenApi::getInstance()->init(config('yibanAppID'),config('yibanAppSecret'),config('yibanCallBackUrl'));
        $iapp = $yibanAPi->getIApp();
        try{
            $info = $iapp->perform();
        }catch (YBException $ex){
            echo $ex->getMessage();
        }
        $token = $info['visit_oauth']['access_token'];//轻应用获取的token
        if ( $token) {
            session('token', $token);
            // $yibanAPi->bind($token);
            return true;
        }
        else{
            return false;
        }
        //  return $yibanAPi;
    }


//校级权限获取用户完整详细信息
public static function schoolBasemsg(){
  if(!isset($_GET['verify_request'])){//未授权跳转到页面;
            redirect(\config('yibanCallBackUrl'));
     }
   if(self::LoginSetToken()){
    $yibanAPi = self::ApiInit();
    $basemsg = $yibanAPi->request('user/verify_me');
    $infomsg = $yibanAPi->request('user/real_me');
    $basemsg['info']['yb_userhead'] = $infomsg['info']['yb_userhead'];
    $basemsg['info']['yb_usernick'] = $infomsg['info']['yb_usernick'];
    $basemsg['info']['yb_money'] = $infomsg['info']['yb_money'];
    $basemsg['info']['yb_sex'] = $infomsg['info']['yb_sex'];
    $basemsg['info']['yb_birthday'] = $infomsg['info']['yb_birthday'];
    $basemsg['info']['yb_exp'] = $infomsg['info']['yb_exp'];
    $basemsg['info']['yb_identity'] = $infomsg['info']['yb_identity'];
    return $basemsg['info'];
   }
}


  //API调用初始化
  public static function ApiInit(){
        $yibanAPi=YBOpenApi::getInstance()>init(config('yibanAppID'),config('yibanAppSecret'),config('yibanCallBackUrl'));
        $yibanAPi->bind(session('token'));
        return $yibanAPi;
    }


}



    /**
     * 接入易班账号登录
     *
     */
    public function yibanLogin()
    {
        /**
         * 登录流程：
         * 1,先API模型获取并设置易班账号Token，返回是否成功设置token状态，true，false
         * 2，API初始化后返回调用接口
         * 2，调用基础信息接口用易班ID实现登录
         */
       if(!isset($_GET['verify_request'])){//未授权跳转到页面;
            $this->redirect(\config('yibanCallBackUrl'));
        }
        if (YibanAPI::LoginSetToken()) {
            $yibanAPi = YibanAPI::ApiInit();
            $yibanmessager = $yibanAPi->request('user/real_me');
            //var_dump($yibanmessager['info']['yb_userid']);
            $yibanInfo = $yibanmessager['info']; //易班返回认证信息数组
            $yibanUser = $this->auth->yibancheck($yibanInfo['yb_userid']);//检查易班ID是否注册本地账号;
            if ($yibanUser) {
                ///监听登录的事件
                $auth = $this->auth;
                Hook::add('user_login_successed', function ($user) use ($auth) {
                    $expire = input('post.keeplogin') ? 30 * 86400 : 0;
                    Cookie::set('uid', $user->id, $expire);
                    Cookie::set('token', $auth->getToken(), $expire);
                });
                $this->auth->direct($yibanUser->id);//直接登录;
                $this->redirect(url('/home'));
            } else {
                ///监听注册的事件
                $auth = $this->auth;
                Hook::add('user_register_successed', function ($user) use ($auth) {
                    Cookie::set('uid', $user->id);
                    Cookie::set('token', $auth->getToken());
                });
                $this->auth->yibanRegister($yibanInfo);
                $this->redirect(url('/home'));
            }
//        }
        }

    }

```


######其他地方使用接入：
```php
   $yibanAPi= YibanAPI::ApiInit();
   $infomsg = $yibanAPi->request('user/real_me');
```


######注意：易班开发者账号appid等配置文件建议写入config.php配置文件
```php
    //易班开发应用ID号----绵师应用大厅
    'yibanAppID'=>'c3e900f710311492',
    //易班开发应用密钥
    'yibanAppSecret'=>'270f4a661b2798eee2adb3a0fe6419cb',
    //易班开发应用回调地址
    'yibanCallBackUrl'=>'http://f.yiban.cn/iapp271724',


```

笔名无香 2019/3/20
未经允许不可擅自更改程序版权信息